#include <iostream>
#include <queue>
using namespace std;
class Musteri{
	string isim;
	int sayi;
	public:
		Musteri(string isim, int sayi) : isim(isim), sayi(sayi) {
		cout << isim << " musterisi olusturuldu" << endl;}
		friend ostream& operator << (ostream&, Musteri&);
}; 

ostream& operator <<(ostream& o, Musteri& m){
	o << m.isim << " " << m.sayi << " adet pide almak istiyor.";
	return o;
}

class Pideci{
	queue<Musteri*> pideKuyrugu;
	public:
		pideSat(){
			if(pideKuyrugu.empty())
			{
				cout << "pide almayi bekleyen kimse yok" << endl;
			}
			else
			{
				Musteri m = *pideKuyrugu.front();
				cout << m << " pidesi verildi" << endl;
				pideKuyrugu.pop();
			}
		}
		pideKuyrugunaGir(Musteri* m)
		{
			if(m)
			{
				pideKuyrugu.push(m);
			}
		}
		friend ostream& operator << (ostream&, Pideci&);
};
ostream& operator <<(ostream& o, Pideci& p){
	if(p.pideKuyrugu.empty())
		o << "pide kuyrugunda kimse yok";
	else
	{
		int i = 1;
		o << "Kuyrukta bekleyen musteriler:" << endl;
		queue<Musteri*> kuyruk = p.pideKuyrugu;
		while(!kuyruk.empty())
		{
			Musteri m = *kuyruk.front();
			o << i << "->" << m << endl;
			kuyruk.pop();
			i++;
		}
		o << endl;
	}
	return o;
}

int main()
{
	Musteri musteri("ufuk", 4);
	Musteri musteri1("ali", 7);
	Musteri musteri2("sena", 1);
	Musteri musteri3("sinem", 2);
	Musteri musteri4("alex", 6);
	Musteri musteri5("hagi", 7);
	Musteri musteri6("sergen", 3);

	Pideci pideci;
	pideci.pideSat();
	pideci.pideKuyrugunaGir(&musteri);
	Pideci p1;
	pideci.pideSat();
	pideci.pideKuyrugunaGir(&musteri1);
	pideci.pideKuyrugunaGir(&musteri2);
	pideci.pideSat();
	cout << pideci;
	pideci.pideKuyrugunaGir(&musteri3);
	pideci.pideSat();
	pideci.pideKuyrugunaGir(&musteri4);
	pideci.pideKuyrugunaGir(&musteri5);
	pideci.pideKuyrugunaGir(&musteri6);

	cout << pideci;
	pideci.pideSat();
	pideci.pideSat();
	pideci.pideSat();
	cout << pideci;


}
