#include <iostream>
#include <queue>
#include <stack>
#include <list>
using namespace std;
int main(){
	/*list<string> wordList;
	wordList.insert("kabak");
	wordList.insert("kazak");
	wordList.insert("ufuk");
	wordList.insert("neden");
	wordList.insert("mentes");*/
	
	bool isPalindrom = true;
	stack<char> stack;
	queue<char> queue;
	string word = "neden";

	for(int i = 0; i < word.length(); i++){
		stack.push(word.at(i));
		queue.push(word.at(i));
	}
	
	while (!stack.empty() && !queue.empty()){
		if (stack.top() != queue.front()) {
			isPalindrom = false;
			break;
		}
		queue.pop();
		stack.pop();
	}
	
	if (isPalindrom)
		cout << word << " kelimesi palindromdur" << endl;
	else
		cout << word << " kelimesi palindrom degildir" << endl;
	
}
